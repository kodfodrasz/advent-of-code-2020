# TODO

## Missing algorithms from the library

### String operations

Edit Distances:

 - Hamming distance (same length)
 - Lewenstein distance (any length)

### Graph algorithms

Shortest path

 - Dijkstra
 - Flowd-Warschal
 - Bellman-Ford (shortest path, generates all paths from one node as a byproduct)

Circle detection

 - Floyd

Topological Sorting

 - Kahn's algorithm

### Lists

 - Shuffle

### Sets

 - Power set
 - Cartesian product[^1]
 - Cartesian product of single set on itself, except diagonal (Does this have a fancy name?)
 - Cartesian product single set on itself, only top triangular matrix, except diagonal (Does this have a fancy name?)

### Number Theory

 - Largest Common Denominator
 - Least Common Multiple
 - Prime factorization
 - Prime sequence generation
 - Primality test
 - Coprimality test

### Combinatorics

 - K-Combination
 - Power set
 - Binomial

### Optimization

 - Hungarian method
 - Backtracking
 
 
 
[^1]: Magyarul/In Hungarian: Descartes-szorzat
