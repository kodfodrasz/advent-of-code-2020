module Kodfodrasz.AoC.Year2020.Day2

open Kodfodrasz.AoC
open System.Text.RegularExpressions

type PasswordPolicy = { Low: int; High: int; Letter: char }

type Password = Password of string

type InputLine = PasswordPolicy * Password

let parseInputLine (line: string) =
  Regex.Match(line, @"^(?<low>\d+)-(?<high>\d+) (?<letter>[a-z]): (?<password>[a-z]+)$")
  |> function
  | m when m.Success ->
      let lowMaybe = m.Groups.["low"].Value |> Parse.parseInt

      let highMaybe =
        m.Groups.["high"].Value |> Parse.parseInt

      let letterMaybe =
        m.Groups.["letter"].Value |> Seq.tryExactlyOne

      (lowMaybe, highMaybe, letterMaybe)
      |||> Option.map3
             (fun low high letter ->
               let policy =
                 { Low = low
                   High = high
                   Letter = letter }

               let password = Password(m.Groups.["password"].Value)
               (policy, password))
  | _ -> None


let parseInput (input: string): Result<InputLine list, string> =
  let parsedMaybe =
    input.Split('\n')
    |> Seq.map String.trim
    |> Seq.filter String.notNullOrWhiteSpace
    |> Seq.map (fun line -> line, parseInputLine line)
    |> Seq.toList

  let errorMaybe =
    parsedMaybe |> Seq.tryFind (snd >> Option.isNone)

  match errorMaybe with
  | None ->
      parsedMaybe
      |> Seq.map (snd >> Option.get)
      |> Seq.toList
      |> Ok
  | Some (line, _) ->
      sprintf "Input line could not be parsed: %s" line
      |> Error

let answer1 (inputlines: InputLine list) =
  let validate (policy, password) =
    match password with
    | Password password ->
        let count =
          password
          |> Seq.filter (fun c -> c = policy.Letter)
          |> Seq.length

        policy.Low <= count && count <= policy.High

  List.filter validate inputlines
  |> List.length
  |> Ok

let validate2 (inputline: InputLine) =
  let (policy, password) = inputline

  match password with
  | Password password ->
      let first = Seq.tryItem (policy.Low - 1) password
      let second = Seq.tryItem (policy.High - 1) password

      (first, second)
      ||> Option.map2
            (fun f s ->
              policy.Letter = f && policy.Letter <> s
              || policy.Letter <> f && policy.Letter = s)
      |> Option.defaultValue false

let answer2 (inputlines: InputLine list) =

  List.filter validate2 inputlines
  |> List.length
  |> Ok

type Solver() =
  inherit SolverBase("Password Philosophy")
  with
    override this.Solve input =
      this.DoSolve parseInput [ answer1; answer2 ] input
