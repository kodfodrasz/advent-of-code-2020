module Kodfodrasz.AoC.Year2020.Day4

open Kodfodrasz.AoC
open System.Text.RegularExpressions

let parseInput (input: string): Result<string list list, string> =
  input.Split('\n')
  |> Seq.map String.trim
  |> Seq.skipWhile String.isNullOrEmpty
  |> Seq.collect (String.split [| ' ' |])
  |> Seq.batchBySeparator ""
  |> Seq.toList
  |> Ok

let answer1 inputlines =
  let parse (claims: string list) =
    claims
    |> Seq.choose
         (fun str ->
           match (String.split [| ':' |] str) with
           | [| k; v |] -> Some(k, v)
           | _ -> None)
    |> dict

  let isValid (claims: System.Collections.Generic.IDictionary<string, string>) =
    let required =
      [ "byr"
        "iyr"
        "eyr"
        "hgt"
        "hcl"
        "ecl"
        "pid" ]

    required |> List.forall (claims.ContainsKey)

  inputlines
  |> List.map parse
  |> List.filter isValid
  |> List.length
  |> Ok

let (|Height|_|) h =
  let matches =
    Regex.Match(h, """^(?<value>\d+)(?<units>cm|in)$""")

  match matches with
  | m when matches.Success ->
      let intval =
        m.Groups.["value"].Value |> Parse.parseInt

      let unit = m.Groups.["units"].Value

      match intval, unit with
      | Some i, "cm" -> Some(i, "cm")
      | Some i, "in" -> Some(i, "in")
      | _ -> None
  | _ -> None

let parse2 (claims: string list) =
  claims
  |> Seq.choose
       (fun str ->
         match (String.split [| ':' |] str) with
         | [| k; v |] -> Some(k, v)
         | _ -> None)
  |> dict

let isValid2 (claims: System.Collections.Generic.IDictionary<string, string>) =
  let requiredKeys =
    [ "byr"
      "iyr"
      "eyr"
      "hgt"
      "hcl"
      "ecl"
      "pid" ]

  let between low high num = low < num && num < high
  let betweeni low high num = low <= num && num <= high

  let validate key (value: string) =
    match key with
    | "byr" ->
        value.Length = 4
        && Parse.parseInt value
           |> Option.map (betweeni 1920 2002)
           |> Option.defaultValue false
    | "iyr" ->
        value.Length = 4
        && Parse.parseInt value
           |> Option.map (betweeni 2010 2020)
           |> Option.defaultValue false
    | "eyr" ->
        value.Length = 4
        && Parse.parseInt value
           |> Option.map (betweeni 2020 2030)
           |> Option.defaultValue false
    | "hgt" ->
        match value with
        | Height (i, "cm") -> i |> betweeni 150 193
        | Height (i, "in") -> i |> betweeni 59 76
        | _ -> false
    | "hcl" -> Regex.IsMatch(value, """^#[0-9a-f]{6}$""")
    | "ecl" ->
        [ "amb"
          "blu"
          "brn"
          "gry"
          "grn"
          "hzl"
          "oth" ]
        |> List.contains value
    | "pid" -> Regex.IsMatch(value, """^[0-9]{9}$""")
    | "cid" -> true
    | _ -> false

  let hasRequiredKeys =
    requiredKeys |> List.forall (claims.ContainsKey)

  let validations =
    claims
    |> Seq.map (fun kv -> (kv.Key, (validate kv.Key kv.Value)))
    |> Seq.toList

  let valuesAreValid = Seq.forall snd validations

  hasRequiredKeys && valuesAreValid

let answer2 inputlines =
  inputlines
  |> List.map parse2
  |> List.filter isValid2
  |> List.length
  |> Ok

type Solver() =
  inherit SolverBase("Passport Processing")
  with
    override this.Solve input =
      this.DoSolve parseInput [ answer1; answer2 ] input
