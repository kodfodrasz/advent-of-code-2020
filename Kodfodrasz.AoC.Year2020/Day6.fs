module Kodfodrasz.AoC.Year2020.Day6

open Kodfodrasz.AoC



let parseInputLine (input: string) = failwith "TODO"

let parseInput (input: string): Result<string list list, string> =
  input.Split('\n')
  |> Seq.map String.trim
  |> Seq.skipWhile String.isNullOrEmpty
  |> Seq.batchBySeparator ""
  |> Seq.toList
  |> Ok


let answer1 (input: string list list) =
  input
  |> Seq.sumBy
       (Seq.collect Seq.toList
        >> Seq.distinct
        >> Seq.length)
  |> Ok


let answer2 input =
  input
  |> Seq.sumBy
       (Seq.map (Seq.toList >> Set.ofSeq)
        >> Set.intersectMany
        >> Seq.length)
  |> Ok


type Solver() =
  inherit SolverBase("Custom Customs")
  with
    override this.Solve input =
      this.DoSolve parseInput [ answer1; answer2 ] input
