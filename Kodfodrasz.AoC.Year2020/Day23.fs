module Kodfodrasz.AoC.Year2020.Day23

open Kodfodrasz.AoC
open System.Collections.Generic

let parseLine (line: string) =
  line.ToCharArray()
  |> Seq.map string
  |> Seq.map Parse.parseInt


let parseInput (input: string): Result<_ list, string> =
  input.Split('\n')
  |> Seq.filter String.notNullOrWhiteSpace
  |> Seq.tryExactlyOne
  |> Option.map (fun line -> (line, parseLine line))
  |> Result.ofOption ("Not a single input line")
  |> Result.bind
       (fun (line, numsMaybe) ->
         if Seq.exists Option.isNone numsMaybe then
           sprintf "Input item could not be parsed: %s" line
           |> Error
         else
           Seq.choose id numsMaybe |> Seq.toList |> Ok)


let answer1 rounds input =
  let list = LinkedList<int>(input)

  let mutable cursor = list.First

  let nextNode (node: LinkedListNode<_>) =
    if node.Next = null then node.List.First else node.Next

  let takeThree () =
    let first = nextNode cursor
    let second = nextNode first
    let third = nextNode second

    cursor.List.Remove(first)
    cursor.List.Remove(second)
    cursor.List.Remove(third)

    [ first; second; third ]

  let findDest () =
    let rec find value =
      let node = list.Find value

      if node <> null then node
      elif (value - 1 < Seq.min list) then find (Seq.max list)
      else find (value - 1)

    find (cursor.Value - 1)

  for i in [ 1 .. rounds ] do
    let pickedUp = takeThree ()
    let dest = findDest ()

    for i in List.rev pickedUp do
      list.AddAfter(dest, i)

    cursor <- nextNode cursor

  let collect =
    let one = list.Find 1
    let mutable item = nextNode one
    let sb = new System.Text.StringBuilder()

    while item <> one do
      sb.Append item.Value |> ignore
      item <- nextNode item

    sb.ToString() |> Parse.parseInt64

  collect
  |> Result.ofOption "Could not parse collected number string"


let answer2 input = failwith "TODO"


type Solver() =
  inherit SolverBase("Crab Cups")
  with
    override this.Solve input =
      this.DoSolve parseInput [ (answer1 100); answer2 ] input
