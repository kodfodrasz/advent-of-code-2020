module Kodfodrasz.AoC.Year2020.Day15

open Kodfodrasz.AoC
open System.Text.RegularExpressions


let parseInput (input: string): Result<_ list, string> =
  let parsedMaybe =
    input.Split('\n')
    |> Seq.collect (String.split [| ',' |])
    |> Seq.map String.trim
    |> Seq.filter String.notNullOrWhiteSpace
    |> Seq.map (fun item -> item, Parse.parseInt item)
    |> Seq.toList

  let errorMaybe =
    parsedMaybe |> Seq.tryFind (snd >> Option.isNone)

  match errorMaybe with
  | None ->
      parsedMaybe
      |> Seq.map (snd >> Option.get)
      |> Seq.toList
      |> Ok
  | Some (line, _) ->
      sprintf "Input item could not be parsed: %s" line
      |> Error


let gameSeq (input: int list) =
  let cache =
    System.Collections.Generic.Dictionary<int, int list>()

  let store index number =
    if cache.ContainsKey number then
      match cache.[number] with
      | [ first ] -> cache.[number] <- [ first; index ]
      | [ first; second ] -> cache.[number] <- [ second; index ]
      | _ -> failwith "should not happen"
    else
      cache.Add(number, List.singleton index)


  seq {
    let mutable lastnum = 0

    let startingNumbers = Seq.indexedFrom 1 input

    for (i, n) in startingNumbers do
      store i n
      lastnum <- n
      yield n

    for i in Seq.startingFrom (input.Length + 1) do

      let num =
        match cache.TryGetValue lastnum with
        | true, [ index ] -> i - 1 - index
        | true, [ first; second ] -> second - first
        | false, _ -> 0
        | _ -> failwith "should not happen"

      store i num
      lastnum <- num
      yield num
  }

let answer1 input =
  let item = 2020

  gameSeq input
  |> Seq.skip (item - 1)
  |> Seq.head
  |> Ok

let answer2 input =
  let item = 30000000

  gameSeq input
  |> Seq.skip (item - 1)
  |> Seq.head
  |> Ok


type Solver() =
  inherit SolverBase("Rambunctious Recitation")
  with
    override this.Solve input =
      this.DoSolve parseInput [ answer1; answer2 ] input
