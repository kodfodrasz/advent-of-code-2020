module Kodfodrasz.AoC.Year2020.Day11

open Kodfodrasz.AoC

type Seat =
  | Floor
  | EmptySeat
  | OccupiedSeat

let parseInputLine line =
  let parseChar c =
    match c with
    | '.' -> Some Floor
    | 'L' -> Some EmptySeat
    | '#' -> Some OccupiedSeat
    | _ -> None

  let items =
    line |> Seq.choose parseChar |> Seq.toList

  if List.length items = String.length line then Some items else None

let parseInput (input: string): Result<_, string> =
  let parsedMaybe =
    input.Split('\n')
    |> Seq.map String.trim
    |> Seq.filter String.notNullOrWhiteSpace
    |> Seq.map (fun line -> line, parseInputLine line)
    |> Seq.toList

  let errorMaybe =
    parsedMaybe |> Seq.tryFind (snd >> Option.isNone)

  match errorMaybe with
  | None ->
      parsedMaybe
      |> Seq.map (snd >> Option.get)
      |> Seq.toList
      |> Ok
  | Some (line, _) ->
      sprintf "Input line could not be parsed: %s" line
      |> Error
  |> Result.map array2D


let answer1 input = failwith "TODO"

let answer2 input = failwith "TODO"

type Solver() =
  inherit SolverBase("Seating System")
  with
    override this.Solve input =
      this.DoSolve parseInput [ answer1; answer2 ] input
