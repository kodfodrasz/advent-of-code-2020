module Kodfodrasz.AoC.Year2020.Day12

open Kodfodrasz.AoC

open System.Text.RegularExpressions

type Heading =
  | East
  | North
  | South
  | West
  member this.Turn instruction =
    match instruction with
    | Left 90
    | Right 270 ->
        match this with
        | East -> North
        | North -> West
        | West -> South
        | South -> East
    | Left 270
    | Right 90 ->
        match this with
        | East -> South
        | South -> West
        | West -> North
        | North -> East
    | Left 180
    | Right 180 ->
        match this with
        | East -> West
        | West -> East
        | North -> South
        | South -> North
    | Left _
    | Right _ -> failwithf "unexpected turn instruction: %A" instruction
    | _ -> this

and NavigationInstruction =
  | North of distance: int
  | South of distance: int
  | West of distance: int
  | East of distance: int
  | Forward of distance: int
  | Left of angle: int
  | Right of angle: int


type ShipPosition = { X: int; Y: int; Heading: Heading }

let parseInputLine line =
  let m =
    System.Text.RegularExpressions.Regex.Match(line, """(?<command>[NSEWLRF])(?<amount>\d+)""")

  if m.Success then
    let commandMaybe = m.Groups.["command"].Value
    let amountMaybe = m.Groups.["amount"].Value

    match (commandMaybe, (Parse.parseInt amountMaybe)) with
    | "N", Some distance -> North distance |> Some
    | "S", Some distance -> South distance |> Some
    | "W", Some distance -> West distance |> Some
    | "E", Some distance -> East distance |> Some
    | "F", Some distance -> Forward distance |> Some
    | "L", Some angle -> Left angle |> Some
    | "R", Some angle -> Right angle |> Some
    | _ -> None
  else
    None

let parseInput (input: string): Result<_ list, string> =
  let parsedMaybe =
    input.Split('\n')
    |> Seq.map String.trim
    |> Seq.filter String.notNullOrWhiteSpace
    |> Seq.map (fun line -> line, parseInputLine line)
    |> Seq.toList

  let errorMaybe =
    parsedMaybe |> Seq.tryFind (snd >> Option.isNone)

  match errorMaybe with
  | None ->
      parsedMaybe
      |> Seq.map (snd >> Option.get)
      |> Seq.toList
      |> Ok
  | Some (line, _) ->
      sprintf "Input line could not be parsed: %s" line
      |> Error

let rec navigate position instruction =
  match instruction with
  | North distance ->
      { position with
          Y = position.Y + distance }
  | South distance ->
      { position with
          Y = position.Y - distance }
  | West distance ->
      { position with
          X = position.X - distance }
  | East distance ->
      { position with
          X = position.X + distance }
  | Forward distance ->
      match position.Heading with
      | Heading.East -> navigate position (East distance)
      | Heading.North -> navigate position (North distance)
      | Heading.West -> navigate position (West distance)
      | Heading.South -> navigate position (South distance)
  | Left angle ->
      { position with
          Heading = position.Heading.Turn instruction }
  | Right angle ->
      { position with
          Heading = position.Heading.Turn instruction }

let answer1 input =
  let position =
    List.fold navigate { X = 0; Y = 0; Heading = Heading.East } input

  (abs position.X) + (abs position.Y) |> Ok

type WaypointPosition = { X: int; Y: int }

type Locations =
  { Ship: ShipPosition
    Waypoint: WaypointPosition }

let rec moveWaypoint (locations: Locations) instruction =
  match instruction with
  | North distance ->
      { locations with
          Waypoint =
            { locations.Waypoint with
                Y = locations.Waypoint.Y + distance } }
  | South distance ->
      { locations with
          Waypoint =
            { locations.Waypoint with
                Y = locations.Waypoint.Y - distance } }
  | West distance ->
      { locations with
          Waypoint =
            { locations.Waypoint with
                X = locations.Waypoint.X - distance } }
  | East distance ->
      { locations with
          Waypoint =
            { locations.Waypoint with
                X = locations.Waypoint.X + distance } }
  | Forward distance ->
      { locations with
          Ship =
            { locations.Ship with
                X =
                  locations.Ship.X
                  + distance * (locations.Waypoint.X)
                Y =
                  locations.Ship.Y
                  + distance * (locations.Waypoint.Y) } }
  | Left angle ->

      let rot =
        match angle with
        | 90 -> [ [ 0; -1 ]; [ 1; 0 ] ]
        | 180 -> [ [ -1; 0 ]; [ 0; -1 ] ]
        | 270 -> [ [ 0; 1 ]; [ -1; 0 ] ]
        | _ -> failwithf "unexpected turn instruction: %A" instruction

      let pos =
        [ locations.Waypoint.X
          locations.Waypoint.Y ]

      let mul =
        [ (pos.[0] * rot.[0].[0]) + (pos.[1] * rot.[0].[1])
          (pos.[0] * rot.[1].[0]) + (pos.[1] * rot.[1].[1]) ]


      { locations with
          Waypoint = { X = mul.[0]; Y = mul.[1] } }
  | Right angle ->

      let rot =
        match angle with
        | 90 -> [ [ 0; 1 ]; [ -1; 0 ] ]
        | 180 -> [ [ -1; 0 ]; [ 0; -1 ] ]
        | 270 -> [ [ 0; -1 ]; [ 1; 0 ] ]
        | _ -> failwithf "unexpected turn instruction: %A" instruction

      let pos =
        [ locations.Waypoint.X
          locations.Waypoint.Y ]

      let mul =
        [ (pos.[0] * rot.[0].[0]) + (pos.[1] * rot.[0].[1])
          (pos.[0] * rot.[1].[0]) + (pos.[1] * rot.[1].[1]) ]

      { locations with
          Waypoint = { X = mul.[0]; Y = mul.[1] } }


let answer2 input =
  let position =
    List.fold
      moveWaypoint
      { Waypoint = { X = 10; Y = 1 }
        Ship = { X = 0; Y = 0; Heading = Heading.East } }
      input

  (abs position.Ship.X) + (abs position.Ship.Y)
  |> Ok



type Solver() =
  inherit SolverBase("Rain Risk")
  with
    override this.Solve input =
      this.DoSolve parseInput [ answer1; answer2 ] input
