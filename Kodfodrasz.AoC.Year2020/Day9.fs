module Kodfodrasz.AoC.Year2020.Day9

open Kodfodrasz.AoC

let parseInputLine = Parse.parseInt64

let parseInput (input: string): Result<_ list, string> =
  let parsedMaybe =
    input.Split('\n')
    |> Seq.map String.trim
    |> Seq.filter String.notNullOrWhiteSpace
    |> Seq.map (fun line -> line, parseInputLine line)
    |> Seq.toList

  let errorMaybe =
    parsedMaybe |> Seq.tryFind (snd >> Option.isNone)

  match errorMaybe with
  | None ->
      parsedMaybe
      |> Seq.map (snd >> Option.get)
      |> Seq.toList
      |> Ok
  | Some (line, _) ->
      sprintf "Input line could not be parsed: %s" line
      |> Error

let answer1 windowSize input =
  let data =
    Seq.zip (List.skip windowSize input) (List.windowed windowSize input)
    |> Seq.toList

  let matchingPair v (a, b) = v = a + b
  let diagonal (a, b) = a = b

  let examineNum (num, window) =
    let pairs =
      Seq.allPairs window window
      |> Seq.filter (diagonal >> not)
      |> Seq.toList

    pairs |> Seq.exists (matchingPair num)

  let filtered = data |> List.filter (examineNum >> not)

  filtered
  |> List.map fst
  |> List.tryExactlyOne
  |> Result.ofOption "Could not find a single matching item in the sequence"

let answer2 windowSize input =
  let invalidNumMaybe = answer1 windowSize input

  // at least 2 sized continous ranges are needed
  let len = List.length input

  let ranges =
    [ 0 .. (len - 2) ]
    |> Seq.collect
         (fun o ->
           let remaining = input.Length - o
           [ 2 .. remaining ] |> Seq.map (fun r -> (o, r)))
    |> Seq.toList

  let slice input (offset, length) =
    input |> Seq.skip offset |> Seq.take length

  let matchingRange num window = num = Seq.sum window
  let calculateWeakness window = Seq.min window + Seq.max window

  let solve invalidNum =
    ranges
    |> Seq.map (slice input)
    |> Seq.filter (matchingRange invalidNum)
    |> Seq.tryExactlyOne
    |> Result.ofOption "Could not find a single matching range in the sequence"
    |> Result.map calculateWeakness

  invalidNumMaybe |> Result.bind solve


type Solver() =
  inherit SolverBase("Encoding Error")
  with
    override this.Solve input =
      this.DoSolve parseInput [ (answer1 25); (answer2 25) ] input
