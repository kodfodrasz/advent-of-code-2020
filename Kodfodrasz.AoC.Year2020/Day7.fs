module Kodfodrasz.AoC.Year2020.Day7

open Kodfodrasz.AoC

open System.Text.RegularExpressions

type Capacity = Capacity of count: int * item: string
type Container = Container of container: string * capacity: Capacity list

let parseInputLine (line: string) =
  let m =
    Regex.Match
      (line,
       """(?<container>\w+ \w+) bags contain (?<contents>no other bags|((?<count>\d+) (?<item>\w+ \w+) bag(s)?(, )?)+).""")

  match m with
  | m when m.Success ->
      let ctr = m.Groups.["container"].Value

      let contents =
        match m.Groups.["contents"] with
        | cts when cts.Value = "no other bags" -> []
        | _ ->
            let counts =
              m.Groups.["count"].Captures
              |> Seq.map (fun c -> Parse.parseInt c.Value)

            let items =
              m.Groups.["item"].Captures
              |> Seq.map (fun c -> c.Value)

            Seq.zip counts items
            |> Seq.choose (fun (cM, i) -> cM |> Option.map (fun c -> Capacity(c, i)))
            |> Seq.toList

      Some(Container(ctr, contents))
  | _ -> None

let parseInput (input: string): Result<Container list, string> =
  let parsedMaybe =
    input.Split('\n')
    |> Seq.map String.trim
    |> Seq.filter String.notNullOrWhiteSpace
    |> Seq.map (fun line -> line, parseInputLine line)
    |> Seq.toList

  let errorMaybe =
    parsedMaybe |> Seq.tryFind (snd >> Option.isNone)

  match errorMaybe with
  | None ->
      parsedMaybe
      |> Seq.map (snd >> Option.get)
      |> Seq.toList
      |> Ok
  | Some (line, _) ->
      sprintf "Input line could not be parsed: %s" line
      |> Error

let edges input =
  input
  |> Seq.collect
       (function
       | Container (container, []) -> [ (container, "<end>", 0) ]
       | Container (container, capacity) ->
           capacity
           |> Seq.map
                (function
                | Capacity (count, item) -> (container, item, count))
           |> Seq.toList)
  |> Seq.toList

let truncate3tuple2 (a, b, c) = (a, b)
let flip2tuple (a, b) = (b, a)

let answer1 input =
  let edges = edges input

  let reversed =
    edges
    |> Seq.map (truncate3tuple2 >> flip2tuple)
    |> Seq.groupBy fst
    |> Seq.map (fun (k, v) -> (k, Seq.map snd v |> Seq.toList))
    |> Map

  let rec walk node (route: string list) (acc: string list list) =
    if List.contains node route then
      // cycle
      (List.rev route) :: acc
    else
      match reversed |> Map.tryFind node with
      | None -> (List.rev (node :: route)) :: acc
      | Some outgoing ->
          let nacc =
            outgoing
            |> List.fold (fun a n -> walk n (node :: route) a) acc

          nacc

  let routes = walk "shiny gold" [] []

  routes
  |> List.collect id
  |> List.distinct
  |> List.filter (fun s -> s <> "shiny gold")
  |> List.length
  |> Ok

let answer2 input =
  let edges =
    edges input
    |> List.filter (fun (f, t, c) -> "<end>" <> t)

  let forward =
    edges
    |> Seq.map truncate3tuple2
    |> Seq.groupBy fst
    |> Seq.map (fun (k, v) -> (k, Seq.map snd v |> Seq.toList))
    |> Map

  let costs =
    edges
    |> Seq.groupBy truncate3tuple2
    |> Seq.map
         (fun (k, v) ->
           let (a, b, c) = Seq.exactlyOne v
           (k, c))
    |> Map

  let rec walk node =
    match forward |> Map.tryFind node with
    | None -> 0
    | Some outgoing ->
        // fsharplint:disable-next-line Hints
        outgoing
        |> List.map
             (fun o ->
               let cost = costs.[(node, o)]
               cost * (1 + walk (o)))
        |> List.fold (+) 0

  walk "shiny gold" |> Ok

type Solver() =
  inherit SolverBase("Handy Haversacks")
  with
    override this.Solve input =
      this.DoSolve parseInput [ answer1; answer2 ] input
