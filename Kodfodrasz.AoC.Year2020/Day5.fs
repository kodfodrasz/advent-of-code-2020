module Kodfodrasz.AoC.Year2020.Day5

open Kodfodrasz.AoC

type Seat =
  { Row: int
    Column: int }
  member this.ID = this.Row * 8 + this.Column

let parseInputLine (input: string): Seat option =
  if input.Length <> 10 then
    None
  else
    let along = input.[0..6] |> Seq.toList
    let cross = input.[7..9] |> Seq.toList

    let rec bpart p1 p2 (range: int * int) steps: int option =
      match steps with
      | [] ->
          let (a, b) = range
          if a = b then Some a else None
      | step :: rest ->
          let (a, b) = range
          let width = (b - a) + 1
          let mid = a + width / 2

          if step = p1 then bpart p1 p2 (a, mid - 1) rest
          elif step = p2 then bpart p1 p2 (mid, b) rest
          else None

    let rowMaybe = bpart 'F' 'B' (0, 127) along
    let colMaybe = bpart 'L' 'R' (0, 7) cross

    (rowMaybe, colMaybe)
    ||> Option.map2 (fun r c -> { Row = r; Column = c }: Seat)

let parseInput (input: string): Result<Seat list, string> =
  let parsedMaybe =
    input.Split('\n')
    |> Seq.map String.trim
    |> Seq.filter String.notNullOrWhiteSpace
    |> Seq.map (fun line -> line, parseInputLine line)
    |> Seq.toList

  let errorMaybe =
    parsedMaybe |> Seq.tryFind (snd >> Option.isNone)

  match errorMaybe with
  | None ->
      parsedMaybe
      |> Seq.map (snd >> Option.get)
      |> Seq.toList
      |> Ok
  | Some (line, _) ->
      sprintf "Input line could not be parsed: %s" line
      |> Error

let answer1 (inputlines: Seat list) =
  let highest = inputlines |> Seq.maxBy (fun s -> s.ID)
  highest.ID |> Ok

let answer2 inputlines =
  let unseen =
    Seq.allPairs [ 0 .. 127 ] [ 0 .. 7 ]
    |> Seq.map (fun (r, c) -> { Row = r; Column = c })
    |> Seq.except inputlines

  let seenIds =
    inputlines |> Seq.map (fun s -> s.ID) |> Set.ofSeq

  let unseenIds = unseen |> Seq.map (fun s -> s.ID)

  unseenIds
  |> Seq.filter (fun u -> seenIds.Contains(u - 1) && seenIds.Contains(u + 1))
  |> Seq.tryExactlyOne
  |> Result.ofOption "Not only a single result left"

type Solver() =
  inherit SolverBase("Binary Boarding")
  with
    override this.Solve input =
      this.DoSolve parseInput [ answer1; answer2 ] input
