module Kodfodrasz.AoC.Year2020.Day3

open Kodfodrasz.AoC
open System.Text.RegularExpressions

let parseInput (input: string): Result<string list, string> =
  input.Split('\n')
  |> Seq.map String.trim
  |> Seq.filter String.notNullOrWhiteSpace
  |> Seq.toList
  |> Ok

let infinite (str: string): char seq =
  seq {
    while true do
      yield! str
  }

let answer1 inputlines =
  inputlines
  |> Seq.skip 1
  |> Seq.indexedFrom 1
  |> Seq.sumBy (fun (i, line) -> if infinite line |> Seq.item (3 * i) = '#' then 1 else 0)
  |> Ok

let steps2 = [ 1, 1; 3, 1; 5, 1; 7, 1; 1, 2 ]

let countTrees inputlines (right, down) =
  let visitedLines down (i, line) =
    if i % down = 0 then Some line else None

  inputlines
  |> Seq.skip 1
  |> Seq.indexedFrom 1
  |> Seq.choose (visitedLines down)
  |> Seq.indexedFrom 1
  |> Seq.filter (fun (i, line) -> infinite line |> Seq.item (right * i) = '#')
  |> Seq.length

let answer2 inputlines =

  let counts =
    steps2 |> Seq.map (countTrees inputlines)

  counts |> Seq.fold ((*)) 1 |> Ok

type Solver() =
  inherit SolverBase("Toboggan Trajectory")
  with
    override this.Solve input =
      this.DoSolve parseInput [ answer1; answer2 ] input
