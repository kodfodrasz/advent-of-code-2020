module Kodfodrasz.AoC.Year2020.Day13

open Kodfodrasz.AoC

open System.Text.RegularExpressions

type Bus =
  | Bus of number: int
  | OutOfService

type Schedule = { Time: int; BusLines: Bus list }

let parseTimeInputLine = Parse.parseInt

let parseBusInputLine (line: string) =
  let parseItem =
    function
    | "x" -> Some OutOfService
    | other -> other |> Parse.parseInt |> Option.map Bus

  let parsedMaybe =
    line.Split(',')
    |> Seq.map String.trim
    |> Seq.filter String.notNullOrWhiteSpace
    |> Seq.map (fun item -> item, parseItem item)
    |> Seq.toList

  let errorMaybe =
    parsedMaybe |> Seq.tryFind (snd >> Option.isNone)

  match errorMaybe with
  | None ->
      parsedMaybe
      |> Seq.map (snd >> Option.get)
      |> Seq.toList
      |> Ok
  | Some (item, _) ->
      sprintf "Input item could not be parsed: %s" item
      |> Error


let parseInput (input: string): Result<_, string> =
  input.Split('\n')
  |> Seq.map String.trim
  |> Seq.filter String.notNullOrWhiteSpace
  |> Seq.toList
  |> function
  | [ timeInputLine; buslinesInputLine ] ->
      let timeMaybe = parseTimeInputLine timeInputLine
      let buslinesMaybe = parseBusInputLine buslinesInputLine

      match (timeMaybe, buslinesMaybe) with
      | Some time, Ok buslines -> { Time = time; BusLines = buslines } |> Ok
      | None, _ -> Error "Unexpected input format: Could not parse timestamp line"
      | _, Error emsg ->
          Error
          <| sprintf "Unexpected input format: Could not parse buslines line: %s" emsg
  | _ -> Error "Unexpected input format: expected two lines"


let answer1 (input: Schedule) =
  let timePulses =
    Seq.initInfinite id
    |> Seq.skipWhile (fun i -> i < input.Time)

  let check time bus =
    match bus with
    | Bus num when 0 = time % num -> Some bus
    | _ -> None

  let firstBuses =
    timePulses
    |> Seq.map
         (fun t ->
           let busesLeaving = input.BusLines |> List.choose (check t)
           (t, busesLeaving))
    |> Seq.find (snd >> List.isEmpty >> not)

  let time = fst firstBuses
  let bus = snd firstBuses |> List.tryExactlyOne

  match bus with
  | Some (Bus b) -> ((time - input.Time) * b) |> int64 |> Ok
  | None -> Error "Not only a single first leaving bus was found"


let answer2 (startoffset: int64) (input: Schedule): Result<int64, string> =
  let busesWithIndices =
    input.BusLines
    |> List.indexed
    |> List.choose
         (fun (i, b) ->
           match b with
           | Bus num -> Some(int64 i, int64 num)
           | _ -> None)

  let (firstBusIndex, firstBusNumber) = busesWithIndices |> List.head

  let countFromLong l s =
    seq {
      let mutable i = l

      while true do
        yield i
        i <- i + s
    }

  let isBusIndexedStartTime bus index time = 0L = (time + index) % (bus)


  let firstStartAligned =
    countFromLong startoffset 1L
    |> Seq.find (isBusIndexedStartTime firstBusNumber firstBusIndex)

  let firstTime =
    countFromLong firstStartAligned firstBusNumber
    |> Seq.find
         (fun time ->
           busesWithIndices
           |> List.forall (fun (i, b) -> isBusIndexedStartTime b i time))

  Ok firstTime

type Solver() =
  inherit SolverBase("Shuttle Search")
  with
    override this.Solve input =
      this.DoSolve parseInput [ answer1; (answer2 100000000000000L) ] input
