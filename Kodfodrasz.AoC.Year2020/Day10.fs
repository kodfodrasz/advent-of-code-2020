module Kodfodrasz.AoC.Year2020.Day10

open Kodfodrasz.AoC

let parseInputLine = Parse.parseInt64

let parseInput (input: string): Result<_ list, string> =
  let parsedMaybe =
    input.Split('\n')
    |> Seq.map String.trim
    |> Seq.filter String.notNullOrWhiteSpace
    |> Seq.map (fun line -> line, parseInputLine line)
    |> Seq.toList

  let errorMaybe =
    parsedMaybe |> Seq.tryFind (snd >> Option.isNone)

  match errorMaybe with
  | None ->
      parsedMaybe
      |> Seq.map (snd >> Option.get)
      |> Seq.toList
      |> Ok
  | Some (line, _) ->
      sprintf "Input line could not be parsed: %s" line
      |> Error

#nowarn "0025"

let answer1 input =
  let outlet = 0L
  let computer = 3L + Seq.max input

  let data =
    [ outlet; computer ] @ input |> List.sort

  let diffs =
    data
    |> List.windowed 2
    |> List.map (fun [ a; b ] -> b - a)

  let ones =
    diffs
    |> Seq.filter (fun d -> d = 1L)
    |> Seq.length
    |> int64

  let threes =
    diffs
    |> Seq.filter (fun d -> d = 3L)
    |> Seq.length
    |> int64

  ones * threes |> Ok

let answer2 input =
  let outlet = 0L
  let computer = 3L + Seq.max input

  let incoming i j = i - 3L <= j && j < i


  let data =
    [ outlet; computer ] @ input |> List.sort

  let incomingPaths =
    data
    |> Seq.map
         (fun i ->
           let c = (data |> List.filter (incoming i))
           (i, c))
    |> Map

  let walk node =
    let cache =
      System.Collections.Concurrent.ConcurrentDictionary<int64, int64>()

    let rec memoize node =
      let rec calculate node =
        if node = outlet then
          1L
        else
          incomingPaths.[node]
          |> List.map memoize
          |> List.sum

      cache.GetOrAdd(node, calculate)

    memoize node

  walk computer |> Ok

type Solver() =
  inherit SolverBase("Adapter Array")
  with
    override this.Solve input =
      this.DoSolve parseInput [ answer1; answer2 ] input
