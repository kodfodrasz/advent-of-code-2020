module Kodfodrasz.AoC.Year2020.Day14

open Kodfodrasz.AoC
open System.Text.RegularExpressions

type MaskElement =
  | Zero
  | One
  | DontCare
  static member DontCareSequence = Seq.initInfinite (fun _ -> DontCare)

type Command =
  | Mask of MaskElement list
  | Memory of address: uint64 * value: uint64
  static member NeutralMask = Mask []

  override this.ToString() =
    match this with
    | Memory (a, v) -> sprintf "mem[%i] = %i" a v
    | Mask m ->
        m
        |> Seq.map
             (fun me ->
               match me with
               | Zero -> "0"
               | One -> "1"
               | DontCare -> "X")
        |> String.join ""
        |> sprintf "mask = %s"

let calculateAndMask (mask: Command) =
  match mask with
  | Mask elements ->
      let mapBit =
        function
        | Zero -> false
        | One -> true
        | DontCare -> true

      let ba =
        Seq.append (Seq.rev elements) MaskElement.DontCareSequence
        |> Seq.take 64
        |> Seq.toArray
        |> Array.map mapBit
        |> Seq.toArray
        |> System.Collections.BitArray

      let target = Array.zeroCreate<byte> 8
      ba.CopyTo(target, 0)

      System.BitConverter.ToUInt64(target, 0)
  | _ -> System.UInt64.MaxValue

let calculateOrMask (mask: Command) =
  match mask with
  | Mask elements ->
      let mapBit =
        function
        | Zero -> false
        | One -> true
        | DontCare -> false

      let ba =
        Seq.append (Seq.rev elements) MaskElement.DontCareSequence
        |> Seq.take 64
        |> Seq.toArray
        |> Array.map mapBit
        |> Seq.toArray
        |> System.Collections.BitArray

      let target = Array.zeroCreate<byte> 8
      ba.CopyTo(target, 0)

      System.BitConverter.ToUInt64(target, 0)
  | _ -> 0UL

let applyMask mask value =
  let mutable cachedMask = Command.NeutralMask
  let mutable calculatedAndMask = System.UInt64.MaxValue
  let mutable calculatedOrMask = 0UL

  if not (obj.ReferenceEquals(cachedMask, mask)) then
    calculatedAndMask <- calculateAndMask mask
    calculatedOrMask <- calculateOrMask mask
    cachedMask <- mask
  else
    ()

  value &&& calculatedAndMask ||| calculatedOrMask


let parseInputLine line =
  let m =
    Regex.Match
      (line,
       """((?<command>mask)\s*=\s*(?<mask>[01X]{36}))|((?<command>mem)[[](?<address>\d+)[]]\s*=\s*(?<value>\d+))""")

  let parseMaskItem c =
    match c with
    | '0' -> Some Zero
    | '1' -> Some One
    | 'X' -> Some DontCare
    | _ -> None

  match m with
  | m when m.Success ->
      match m.Groups.["command"].Value with
      | "mask" ->
          let maskMaybe = m.Groups.["mask"].Value

          let maskParsedItems =
            maskMaybe
            |> Seq.choose parseMaskItem
            |> Seq.toList

          if maskParsedItems.Length = maskMaybe.Length
          then Some(Mask maskParsedItems)
          else None

      | "mem" ->
          let addressMaybe =
            m.Groups.["address"].Value |> Parse.parseUInt64

          let valueMaybe =
            m.Groups.["value"].Value |> Parse.parseUInt64

          match addressMaybe, valueMaybe with
          | Some addr, Some value -> Some(Memory(addr, value))
          | _ -> None
      | _ -> None

let parseInput (input: string): Result<_ list, string> =
  let parsedMaybe =
    input.Split('\n')
    |> Seq.map String.trim
    |> Seq.filter String.notNullOrWhiteSpace
    |> Seq.map (fun line -> line, parseInputLine line)
    |> Seq.toList

  let errorMaybe =
    parsedMaybe |> Seq.tryFind (snd >> Option.isNone)

  match errorMaybe with
  | None ->
      parsedMaybe
      |> Seq.map (snd >> Option.get)
      |> Seq.toList
      |> Ok
  | Some (line, _) ->
      sprintf "Input line could not be parsed: %s" line
      |> Error


let answer1 input =
  let mem =
    System.Collections.Generic.Dictionary<uint64, uint64>()

  let mutable mask = Command.NeutralMask

  for cmd in input do
    match cmd with
    | Mask m -> mask <- Mask m
    | Memory (addr, value) ->
        let maskedVal = applyMask mask value
        if mem.ContainsKey addr then mem.[addr] <- maskedVal else mem.Add(addr, maskedVal)

  mem.Values |> Seq.sum |> Ok


let binstr (n: uint64) =
  System.Convert.ToString((int64 n), 2)
  |> sprintf "%36s"
  |> String.replace " " "0"

let applyMask2 originalMask value mask =
  let calculatedAndBitmask = calculateAndMask mask
  let calculatedOrBitmask = calculateOrMask mask
  let unpermutedAndBitmask = ~~~(calculateAndMask originalMask)

  let res =
    (value
     &&& (calculatedOrBitmask ^^^ unpermutedAndBitmask)
     ||| calculatedOrBitmask)
    &&& 0xfffffffffUL

  res

let floatMask mask =
  let rec permuteSingle acc maskItems =
    match maskItems with
    | [] -> acc |> List.map (List.rev >> Mask)
    | DontCare :: tail ->
        let defined =
          match acc with
          | [] -> [ [ Zero ]; [ One ] ]
          | nonempty ->
              let oned =
                nonempty |> List.map (fun comms -> One :: comms)

              let zeroed =
                nonempty |> List.map (fun comms -> Zero :: comms)

              zeroed @ oned

        permuteSingle defined tail
    | definite :: tail ->
        let defined =
          match acc with
          | [] -> [ [ definite ] ]
          | nonempty ->
              nonempty
              |> List.map (fun comms -> definite :: comms)

        permuteSingle defined tail

  match mask with
  | Mask m -> permuteSingle [] m
  | _ -> []
  |> List.sort

let answer2 input =
  let mem =
    System.Collections.Generic.Dictionary<uint64, uint64>()

  let writeMem addr value =
    if mem.ContainsKey addr then mem.[addr] <- value else mem.Add(addr, value)

  let mutable mask = Command.NeutralMask
  let mutable permutedMasks = [ Command.NeutralMask ]

  for cmd in input do
    match cmd with
    | Mask m ->
        mask <- Mask m
        permutedMasks <- floatMask (Mask m)
    | Memory (addr, value) ->
        let maskedAddresses =
          permutedMasks
          |> List.map (applyMask2 mask addr)
          |> List.sort

        for maskedAddr in maskedAddresses do
          writeMem maskedAddr value

  mem.Values |> Seq.sum |> Ok

type Solver() =
  inherit SolverBase("Docking Data")
  with
    override this.Solve input =
      this.DoSolve parseInput [ answer1; answer2 ] input
