module Kodfodrasz.AoC.Year2020.Day8

open Kodfodrasz.AoC

type Op =
  | Nop of ignored: int
  | Acc of increment: int
  | Jmp of offset: int

let parseInputLine (input: string) =
  match input.Split(" ") with
  | [| "nop"; ignored |] -> ignored |> Parse.parseInt |> Option.map Nop
  | [| "acc"; inc |] -> inc |> Parse.parseInt |> Option.map Acc
  | [| "jmp"; offs |] -> offs |> Parse.parseInt |> Option.map Jmp
  | _ -> None

let parseInput (input: string): Result<Op list, string> =
  let parsedMaybe =
    input.Split('\n')
    |> Seq.map String.trim
    |> Seq.filter String.notNullOrWhiteSpace
    |> Seq.map (fun line -> line, parseInputLine line)
    |> Seq.toList

  let errorMaybe =
    parsedMaybe |> Seq.tryFind (snd >> Option.isNone)

  match errorMaybe with
  | None ->
      parsedMaybe
      |> Seq.map (snd >> Option.get)
      |> Seq.toList
      |> Ok
  | Some (line, _) ->
      sprintf "Input line could not be parsed: %s" line
      |> Error

let answer1 (input: Op list) =
  let rec run acc (pc: int) (visited: Set<int>) ops =
    if visited.Contains(pc) then
      Ok acc
    else
      match ops |> Array.tryItem pc with
      | Some (Nop _) -> run acc (pc + 1) (visited.Add pc) ops
      | Some (Acc i) -> run (acc + i) (pc + 1) (visited.Add pc) ops
      | Some (Jmp o) -> run acc (pc + o) (visited.Add pc) ops
      | None ->
          Error
          <| sprintf "Invalid instruction offset! pc=%i" pc

  input |> List.toArray |> run 0 0 Set.empty<int>

let answer2 input =
  let rec run acc (pc: int) (visited: Set<int>) ops =
    if visited.Contains(pc) then
      Ok None
    else
      match ops |> Array.tryItem pc with
      | Some (Nop _) -> run acc (pc + 1) (visited.Add pc) ops
      | Some (Acc i) -> run (acc + i) (pc + 1) (visited.Add pc) ops
      | Some (Jmp o) -> run acc (pc + o) (visited.Add pc) ops
      | None ->
          if pc = ops.Length then
            Ok(Some acc)
          else
            Error
            <| sprintf "Invalid instruction offset! pc=%i" pc

  let candicates =
    input
    |> Seq.indexed
    |> Seq.choose
         (function
         | (i, Nop o) -> Some(i, Jmp o)
         | (i, Jmp o) -> Some(i, Nop o)
         | _ -> None)
    |> Seq.toArray

  let runCandidate (idx: int, op: Op) =
    let ops = List.toArray input
    ops.[idx] <- op
    run 0 0 Set.empty<int> ops

  candicates
  |> Seq.map runCandidate
  |> Seq.find
       (function
       | Error e -> true
       | Ok (Some acc) -> true
       | _ -> false)
  |> Result.map Option.get


type Solver() =
  inherit SolverBase("Handheld Halting")
  with
    override this.Solve input =
      this.DoSolve parseInput [ answer1; answer2 ] input
