// fsharplint:disable ParameterNames
module Kodfodrasz.AoC.Year2020.Tests.Day10Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day10


let exampleInput = """
16
10
15
5
1
11
7
19
6
12
4
"""

let exampleWindowSize = 5

[<Fact>]
let ``Parsing example input`` () =
  let expected: Result<_ list, string> =
    Ok [ 16L
         10L
         15L
         5L
         1L
         11L
         7L
         19L
         6L
         12L
         4L ]


  test
    <@ let actual = parseInput exampleInput
       actual = expected @>

[<Fact>]
let ``Part 1 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 35L

  test <@ answer1 input = Ok expected @>

let exampleInput2 = """
28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3
"""

[<Fact>]
let ``Part 1 answer for example input 2 test`` () =
  let input = parseInput exampleInput2 |> Result.get
  let expected = 220L

  test <@ answer1 input = Ok expected @>

[<Fact>]
let ``Part 2 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 8L

  test <@ answer2 input = Ok expected @>

[<Fact>]
let ``Part 2 answer for example input 2 test`` () =
  let input = parseInput exampleInput2 |> Result.get
  let expected = 19208L

  test <@ answer2 input = Ok expected @>
