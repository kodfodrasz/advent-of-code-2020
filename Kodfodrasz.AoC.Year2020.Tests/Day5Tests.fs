// fsharplint:disable ParameterNames
module Kodfodrasz.AoC.Year2020.Tests.Day5Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day5


[<Fact>]
let ``Parsing example input lines`` () =
  test <@ Some { Row = 44; Column = 5 } = parseInputLine "FBFBBFFRLR" @>
  test <@ Some { Row = 70; Column = 7 } = parseInputLine "BFFFBBFRRR" @>
  test <@ Some { Row = 14; Column = 7 } = parseInputLine "FFFBBBFRRR" @>
  test <@ Some { Row = 102; Column = 4 } = parseInputLine "BBFFBBFRLL" @>

[<Fact>]
let ``Parsing boundary input lines`` () =
  test <@ Some { Row = 0; Column = 0 } = parseInputLine "FFFFFFFLLL" @>
  test <@ Some { Row = 127; Column = 7 } = parseInputLine "BBBBBBBRRR" @>


[<Fact>]
let ``Calculating seat IDs`` () =
  test <@ 357 = { Row = 44; Column = 5 }.ID @>
  test <@ 567 = { Row = 70; Column = 7 }.ID @>
  test <@ 119 = { Row = 14; Column = 7 }.ID @>
  test <@ 820 = { Row = 102; Column = 4 }.ID @>
