// fsharplint:disable ParameterNames
module Kodfodrasz.AoC.Year2020.Tests.Day11Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day11


let exampleInputSequence =
  [ """
  L.LL.LL.LL
  LLLLLLL.LL
  L.L.L..L..
  LLLL.LL.LL
  L.LL.LL.LL
  L.LLLLL.LL
  ..L.L.....
  LLLLLLLLLL
  L.LLLLLL.L
  L.LLLLL.LL
  """

    """
  #.##.##.##
  #######.##
  #.#.#..#..
  ####.##.##
  #.##.##.##
  #.#####.##
  ..#.#.....
  ##########
  #.######.#
  #.#####.##
  """

    """
  #.LL.L#.##
  #LLLLLL.L#
  L.L.L..L..
  #LLL.LL.L#
  #.LL.LL.LL
  #.LLLL#.##
  ..L.L.....
  #LLLLLLLL#
  #.LLLLLL.L
  #.#LLLL.##
  """

    """
  #.##.L#.##
  #L###LL.L#
  L.#.#..#..
  #L##.##.L#
  #.##.LL.LL
  #.###L#.##
  ..#.#.....
  #L######L#
  #.LL###L.L
  #.#L###.##
  """

    """
  #.#L.L#.##
  #LLL#LL.L#
  L.L.L..#..
  #LLL.##.L#
  #.LL.LL.LL
  #.LL#L#.##
  ..L.L.....
  #L#LLLL#L#
  #.LLLLLL.L
  #.#L#L#.##
  """

    """
  #.#L.L#.##
  #LLL#LL.L#
  L.#.L..#..
  #L##.##.L#
  #.#L.LL.LL
  #.#L#L#.##
  ..L.L.....
  #L#L##L#L#
  #.LLLLLL.L
  #.#L#L#.##
  """ ]

let exampleInput = List.head exampleInputSequence

[<Fact>]
let ``Parsing example input`` () =
  let expected: Result<_, string> =
    [ [ EmptySeat
        Floor
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        EmptySeat ]
      [ EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        EmptySeat ]
      [ EmptySeat
        Floor
        EmptySeat
        Floor
        EmptySeat
        Floor
        Floor
        EmptySeat
        Floor
        Floor ]
      [ EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        EmptySeat ]
      [ EmptySeat
        Floor
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        EmptySeat ]
      [ EmptySeat
        Floor
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        EmptySeat ]
      [ Floor
        Floor
        EmptySeat
        Floor
        EmptySeat
        Floor
        Floor
        Floor
        Floor
        Floor ]
      [ EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat ]
      [ EmptySeat
        Floor
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        Floor
        EmptySeat ]
      [ EmptySeat
        Floor
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        EmptySeat ] ]
    |> array2D
    |> Ok

  test
    <@ let actual = parseInput exampleInput
       actual = expected @>

[<Fact>]
let ``Parsing example input (after second update)`` () =
  let expected: Result<_, string> =
    [ [ OccupiedSeat
        Floor
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        OccupiedSeat
        Floor
        OccupiedSeat
        OccupiedSeat ]
      [ OccupiedSeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        OccupiedSeat ]
      [ EmptySeat
        Floor
        EmptySeat
        Floor
        EmptySeat
        Floor
        Floor
        EmptySeat
        Floor
        Floor ]
      [ OccupiedSeat
        EmptySeat
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        OccupiedSeat ]
      [ OccupiedSeat
        Floor
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        EmptySeat
        Floor
        EmptySeat
        EmptySeat ]
      [ OccupiedSeat
        Floor
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        OccupiedSeat
        Floor
        OccupiedSeat
        OccupiedSeat ]
      [ Floor
        Floor
        EmptySeat
        Floor
        EmptySeat
        Floor
        Floor
        Floor
        Floor
        Floor ]
      [ OccupiedSeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        OccupiedSeat ]
      [ OccupiedSeat
        Floor
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        Floor
        EmptySeat ]
      [ OccupiedSeat
        Floor
        OccupiedSeat
        EmptySeat
        EmptySeat
        EmptySeat
        EmptySeat
        Floor
        OccupiedSeat
        OccupiedSeat ] ]
    |> array2D
    |> Ok


  test
    <@ let actual = parseInput exampleInputSequence.[2]
       actual = expected @>


//[<Fact>]
//let ``Part 1 answer for example input test`` () =
//  let input = parseInput exampleInput |> Result.get
//  let expected = 35L

//  test <@ answer1 input = Ok expected @>

//[<Fact>]
//let ``Part 2 answer for example input test`` () =
//  let input = parseInput exampleInput |> Result.get
//  let expected = 8L

//  test <@ answer2 input = Ok expected @>
