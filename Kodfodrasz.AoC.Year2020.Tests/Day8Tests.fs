// fsharplint:disable ParameterNames
module Kodfodrasz.AoC.Year2020.Tests.Day8Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day8


let exampleInput = """
nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6
"""


[<Fact>]
let ``Parsing example input`` () =
  let expected: Result<Op list, string> =
    Ok [ Nop 0
         Acc 1
         Jmp 4
         Acc 3
         Jmp -3
         Acc -99
         Acc 1
         Jmp -4
         Acc 6 ]

  test
    <@ let actual = parseInput exampleInput
       actual = expected @>

[<Fact>]
let ``Part 1 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 5

  test <@ answer1 input = Ok expected @>

[<Fact>]
let ``Part 2 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 8

  test <@ answer2 input = Ok expected @>
