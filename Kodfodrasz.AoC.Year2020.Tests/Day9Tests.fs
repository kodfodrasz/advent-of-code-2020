// fsharplint:disable ParameterNames
module Kodfodrasz.AoC.Year2020.Tests.Day9Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day9


let exampleInput = """
35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576
"""

let exampleWindowSize = 5

[<Fact>]
let ``Parsing example input`` () =
  let expected: Result<int64 list, string> =
    Ok [ 35L
         20L
         15L
         25L
         47L
         40L
         62L
         55L
         65L
         95L
         102L
         117L
         150L
         182L
         127L
         219L
         299L
         277L
         309L
         576L ]


  test
    <@ let actual = parseInput exampleInput
       actual = expected @>

[<Fact>]
let ``Part 1 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 127L

  test <@ answer1 5 input = Ok expected @>

[<Fact>]
let ``Part 2 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 62L

  test <@ answer2 5 input = Ok expected @>
