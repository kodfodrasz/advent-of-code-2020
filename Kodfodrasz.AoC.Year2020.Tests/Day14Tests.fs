// fsharplint:disable ParameterNames
module Kodfodrasz.AoC.Year2020.Tests.Day14Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day14


let exampleInput = """
mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0
"""

[<Fact>]
let ``Parsing example input`` () =
  let expected: Result<_, string> =
    Ok [ Mask
           ([ DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              DontCare
              One
              DontCare
              DontCare
              DontCare
              DontCare
              Zero
              DontCare ])
         Memory(8UL, 11UL)
         Memory(7UL, 101UL)
         Memory(8UL, 0UL) ]

  test
    <@ let actual = parseInput exampleInput
       actual = expected @>

[<Theory>]
[<InlineData("mask = XX1XXXX0XXX0XXXXXXXXXXXXX0XXX1XXXX0X",
             0b1111111111111111111111111111111111101110111111111111101111111101UL)>]
[<InlineData("mask = 10XXX0XXXX0XXX0X1XX1XXX0X0X0101XXX00",
             0b1111111111111111111111111111101110111101110111111110101010111100UL)>]
let ``calculateAndMask helper function tests`` maskLine expected =
  let mask = parseInputLine maskLine |> Option.get

  let actual = calculateAndMask mask

  //let expectedStr =
  //  System.Convert.ToString(int64 expected, toBase = 2)
  //let actualStr =
  //  System.Convert.ToString(int64 actual, toBase = 2)

  test <@ expected = actual @>

[<Theory>]
[<InlineData("mask = XX1XXXX0XXX0XXXXXXXXXXXXX0XXX1XXXX0X",
             0b0000000000000000000000000000001000000000000000000000000001000000UL)>]
[<InlineData("mask = 10XXX0XXXX0XXX0X1XX1XXX0X0X0101XXX00",
             0b0000000000000000000000000000100000000000000010010000000010100000UL)>]
let ``calculateOrMask helper function tests`` maskLine expected =
  let mask = parseInputLine maskLine |> Option.get

  let actual = calculateOrMask mask

  //let expectedStr =
  //  System.Convert.ToString(int64 expected, toBase = 2)
  //let actualStr =
  //  System.Convert.ToString(int64 actual, toBase = 2)

  test <@ expected = actual @>


[<Theory>]
[<InlineData("mask = XX1XXXX0XXX0XXXXXXXXXXXXX0XXX1XXXX0X",
             0UL,
             0b0000000000000000000000000000001000000000000000000000000001000000UL)>]
[<InlineData("mask = 10XXX0XXXX0XXX0X1XX1XXX0X0X0101XXX00",
             0UL,
             0b0000000000000000000000000000100000000000000010010000000010100000UL)>]
[<InlineData("mask = XX1XXXX0XXX0XXXXXXXXXXXXX0XXX1XXXX0X",
             0xF_FFFF_FFFFUL,
             0b0000000000000000000000000000111111101110111111111111101111111101UL)>]
[<InlineData("mask = 10XXX0XXXX0XXX0X1XX1XXX0X0X0101XXX00",
             0xF_FFFF_FFFFUL,
             0b0000000000000000000000000000101110111101110111111110101010111100UL)>]

[<InlineData("mask = XX1XXXX0XXX0XXXXXXXXXXXXX0XXX1XXXX0X",
             System.UInt64.MaxValue,
             0b1111111111111111111111111111111111101110111111111111101111111101UL)>]
[<InlineData("mask = 10XXX0XXXX0XXX0X1XX1XXX0X0X0101XXX00",
             System.UInt64.MaxValue,
             0b1111111111111111111111111111101110111101110111111110101010111100UL)>]
let ``applyMask helper function tests`` maskLine value expected =
  let mask = parseInputLine maskLine |> Option.get

  let actual = applyMask mask value

  //let expectedStr =
  //  System.Convert.ToString(int64 expected, toBase = 2)
  //let actualStr =
  //  System.Convert.ToString(int64 actual, toBase = 2)

  test <@ expected = actual @>

[<Fact>]
let ``Part 1 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 165UL

  test <@ answer1 input = Ok expected @>


[<Fact>]
let ``floatMask 2 helper function test 1`` () =
  let input =
    Mask [ Zero
           DontCare
           One
           DontCare
           One ]

  let expected =
    List.sort [ Mask [ Zero; Zero; One; Zero; One ]
                Mask [ Zero; Zero; One; One; One ]
                Mask [ Zero; One; One; Zero; One ]
                Mask [ Zero; One; One; One; One ] ]

  test <@ expected = floatMask input @>

[<Fact>]
let ``floatMask 2 helper function test 2`` () =
  let maskString =
    "mask = XX101010X10X100X11101100000X101X1011"

  let mask =
    maskString |> parseInputLine |> Option.get

  let floated = floatMask mask

  let expectedLength =
    let numX =
      Seq.filter (fun c -> c = 'X') maskString
      |> Seq.length

    System.Math.Pow(2.0, float numX) |> int

  test <@ expectedLength = floated.Length @>

[<Fact>]
let ``floatMask 2 helper function test 3`` () =
  let input =
    Mask [ DontCare
           Zero
           One
           DontCare
           One ]

  let expected =
    List.sort [ Mask [ Zero; Zero; One; Zero; One ]
                Mask [ Zero; Zero; One; One; One ]
                Mask [ One; Zero; One; Zero; One ]
                Mask [ One; Zero; One; One; One ] ]

  test <@ expected = floatMask input @>

let exampleInput_Part2 = """
mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1
"""

[<Fact>]
let ``Part 2 answer for example input test`` () =
  let input =
    parseInput exampleInput_Part2 |> Result.get

  let expected = 208UL

  test <@ answer2 input = Ok expected @>
