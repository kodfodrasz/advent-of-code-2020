// fsharplint:disable ParameterNames
module Kodfodrasz.AoC.Year2020.Tests.Day23Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day23


let exampleInput = """
389125467
"""

[<Fact>]
let ``Parsing example input`` () =
  let expected: Result<_, string> = Ok [ 3; 8; 9; 1; 2; 5; 4; 6; 7 ]

  test
    <@ let actual = parseInput exampleInput
       actual = expected @>


[<Fact>]
let ``Part 1 answer for example input test after 10 rounds`` () =
  let input = parseInput exampleInput |> Result.get

  let expected = 92658374L

  test <@ answer1 10 input = Ok expected @>


[<Fact>]
let ``Part 1 answer for example input test after 100 rounds`` () =
  let input = parseInput exampleInput |> Result.get

  let expected = 67384529L

  test <@ answer1 100 input = Ok expected @>


//[<Fact>]
//let ``Part 2 answer for example input test`` () =
//  let input = parseInput exampleInput |> Result.get
//  let expected = 1068781L

//  test <@ answer2 1L input = Ok expected @>
