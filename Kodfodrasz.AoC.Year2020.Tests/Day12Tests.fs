// fsharplint:disable ParameterNames
module Kodfodrasz.AoC.Year2020.Tests.Day12Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day12


let exampleInput = """
F10
N3
F7
R90
F11
"""

[<Fact>]
let ``Parsing example input`` () =
  let expected: Result<_ list, string> =
    Ok [ Forward 10
         North 3
         Forward 7
         Right 90
         Forward 11 ]

  test
    <@ let actual = parseInput exampleInput
       actual = expected @>

[<Fact>]
let ``Part 1 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 25

  test <@ answer1 input = Ok expected @>

[<Fact>]
let ``Part 2 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 286

  test <@ answer2 input = Ok expected @>

[<Fact>]
let ``Part 2 helper function moveWaypoint test`` () =

  let initialPosition =
    { Waypoint = { X = 2; Y = 1 }
      Ship = { X = 0; Y = 0; Heading = Heading.East } }

  test
    <@ let res = moveWaypoint initialPosition (Right 90)
       res.Waypoint = { X = 1; Y = -2 } @>

  test
    <@ let res = moveWaypoint initialPosition (Right 180)

       res.Waypoint = { X = -2; Y = -1 } @>

  test
    <@ let res = moveWaypoint initialPosition (Right 270)

       res.Waypoint = { X = -1; Y = 2 } @>

  test
    <@ let res = moveWaypoint initialPosition (Left 90)
       res.Waypoint = { X = -1; Y = 2 } @>

  test
    <@ let res = moveWaypoint initialPosition (Left 180)
       res.Waypoint = { X = -2; Y = -1 } @>

  test
    <@ let res = moveWaypoint initialPosition (Left 270)
       res.Waypoint = { X = 1; Y = -2 } @>
