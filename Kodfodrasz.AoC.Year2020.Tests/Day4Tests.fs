// fsharplint:disable ParameterNames
module Kodfodrasz.AoC.Year2020.Tests.Day4Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day4


let exampleInput = """
  ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
  byr:1937 iyr:2017 cid:147 hgt:183cm

  iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
  hcl:#cfa07d byr:1929

  hcl:#ae17e1 iyr:2013
  eyr:2024
  ecl:brn pid:760753108 byr:1931
  hgt:179cm

  hcl:#cfa07d eyr:2025 pid:166559648
  iyr:2011 ecl:brn hgt:59in
  """


[<Fact>]
let ``Parsing example input`` () =
  let expected: Result<string list list, string> =
    Ok [ [ "ecl:gry"
           "pid:860033327"
           "eyr:2020"
           "hcl:#fffffd"
           "byr:1937"
           "iyr:2017"
           "cid:147"
           "hgt:183cm" ]
         [ "iyr:2013"
           "ecl:amb"
           "cid:350"
           "eyr:2023"
           "pid:028048884"
           "hcl:#cfa07d"
           "byr:1929" ]
         [ "hcl:#ae17e1"
           "iyr:2013"
           "eyr:2024"
           "ecl:brn"
           "pid:760753108"
           "byr:1931"
           "hgt:179cm" ]
         [ "hcl:#cfa07d"
           "eyr:2025"
           "pid:166559648"
           "iyr:2011"
           "ecl:brn"
           "hgt:59in" ] ]

  test
    <@ let actual = parseInput exampleInput
       actual = expected @>

[<Fact>]
let ``Part 1 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 2

  test <@ answer1 input = Ok expected @>


let exampleInput_part2_invalid = """
  eyr:1972 cid:100
  hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

  iyr:2019
  hcl:#602927 eyr:1967 hgt:170cm
  ecl:grn pid:012533040 byr:1946

  hcl:dab227 iyr:2012
  ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

  hgt:59cm ecl:zzz
  eyr:2038 hcl:74454a iyr:2023
  pid:3556412378 byr:2007
  """


[<Fact>]
let ``Part 2 validation checker helper test (invalid inputs)`` () =
  let input =
    parseInput exampleInput_part2_invalid
    |> Result.get
    |> List.map parse2

  let validations = input |> List.map isValid2

  test <@ List.forall not validations @>


let exampleInput_part2_valid = """
  pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
  hcl:#623a2f

  eyr:2029 ecl:blu cid:129 byr:1989
  iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

  hcl:#888785
  hgt:164cm byr:2001 iyr:2015 cid:88
  pid:545766238 ecl:hzl
  eyr:2022

  iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719
  """


[<Fact>]
let ``Part 2 validation checker helper test (valid inputs)`` () =
  let input =
    parseInput exampleInput_part2_valid
    |> Result.get
    |> List.map parse2

  let validations = input |> List.map isValid2

  test <@ List.forall id validations @>
