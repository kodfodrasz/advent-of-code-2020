module Kodfodrasz.AoC.Year2020.Tests.Day3Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day3


let exampleInput = """
  ..##.......
  #...#...#..
  .#....#..#.
  ..#.#...#.#
  .#...##..#.
  ..#.##.....
  .#.#.#....#
  .#........#
  #.##...#...
  #...##....#
  .#..#...#.#
  """

[<Fact>]
let ``Part 1 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 7

  test <@ answer1 input = Ok expected @>

[<Fact>]
let ``Part 2 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 336

  test <@ answer2 input = Ok expected @>

[<Fact>]
let ``Part 2 tree counter helper for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = [ 2; 7; 3; 4; 2 ]

  let counts =
    steps2 |> Seq.map (countTrees input) |> Seq.toList

  test <@ counts = expected @>
