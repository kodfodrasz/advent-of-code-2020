// fsharplint:disable ParameterNames
module Kodfodrasz.AoC.Year2020.Tests.Day13Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day13


let exampleInput = """
939
7,13,x,x,59,x,31,19
"""

[<Fact>]
let ``Parsing example input`` () =
  let expected: Result<_, string> =
    Ok
      { Time = 939
        BusLines =
          [ Bus 7
            Bus 13
            OutOfService
            OutOfService
            Bus 59
            OutOfService
            Bus 31
            Bus 19 ] }

  test
    <@ let actual = parseInput exampleInput
       actual = expected @>

[<Fact>]
let ``Part 1 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 295L

  test <@ answer1 input = Ok expected @>

[<Fact>]
let ``Part 2 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 1068781L

  test <@ answer2 1L input = Ok expected @>
