// fsharplint:disable ParameterNames
module Kodfodrasz.AoC.Year2020.Tests.Day6Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day6


let exampleInput = """
  abc

  a
  b
  c

  ab
  ac

  a
  a
  a
  a

  b
"""


[<Fact>]
let ``Parsing example input`` () =
  let expected: Result<string list list, string> =
    Ok [ [ "abc" ]
         [ "a"; "b"; "c" ]
         [ "ab"; "ac" ]
         [ "a"; "a"; "a"; "a" ]
         [ "b" ] ]

  test
    <@ let actual = parseInput exampleInput
       actual = expected @>

[<Fact>]
let ``Part 1 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 11

  test <@ answer1 input = Ok expected @>

[<Fact>]
let ``Part 2 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 6

  test <@ answer2 input = Ok expected @>
