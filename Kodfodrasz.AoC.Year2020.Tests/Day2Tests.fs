module Kodfodrasz.AoC.Year2020.Tests.Day2Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day2


let exampleInput = """
  1-3 a: abcde
  1-3 b: cdefg
  2-9 c: ccccccccc
  """

[<Fact>]
let ``Parsing example input`` () =
  let expected: Result<InputLine list, string> =
    Ok [ { Low = 1; High = 3; Letter = 'a' }, Password "abcde"
         { Low = 1; High = 3; Letter = 'b' }, Password "cdefg"
         { Low = 2; High = 9; Letter = 'c' }, Password "ccccccccc" ]

  test
    <@ let actual = parseInput exampleInput
       actual = expected @>

[<Fact>]
let ``Part 2 helper for password validation test`` () =
  let expected: Result<Password list, string> = Ok [ Password "abcde" ]

  let input = parseInput exampleInput

  let actual =
    input
    |> Result.map (List.filter validate2 >> List.map snd)

  test <@ actual = expected @>

[<Fact>]
let ``Part 2 helper for password validation test 2`` () =

  test <@ validate2 ({ Low = 1; High = 5; Letter = 'a' }, Password "abcde") @>
  test <@ not (validate2 ({ Low = 2; High = 5; Letter = 'a' }, Password "abcde")) @>


[<Fact>]
let ``Part 1 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 2

  test <@ answer1 input = Ok expected @>


[<Fact>]
let ``Part 2 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 1

  test <@ answer2 input = Ok expected @>
