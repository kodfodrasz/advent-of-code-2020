// fsharplint:disable ParameterNames
module Kodfodrasz.AoC.Year2020.Tests.Day7Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day7


let exampleInput = """
  light red bags contain 1 bright white bag, 2 muted yellow bags.
  dark orange bags contain 3 bright white bags, 4 muted yellow bags.
  bright white bags contain 1 shiny gold bag.
  muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
  shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
  dark olive bags contain 3 faded blue bags, 4 dotted black bags.
  vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
  faded blue bags contain no other bags.
  dotted black bags contain no other bags.
"""


[<Fact>]
let ``Parsing example input`` () =
  let expected: Result<Container list, string> =
    Ok [ Container
           ("light red",
            [ Capacity(1, "bright white")
              Capacity(2, "muted yellow") ])
         Container
           ("dark orange",
            [ Capacity(3, "bright white")
              Capacity(4, "muted yellow") ])
         Container("bright white", [ Capacity(1, "shiny gold") ])
         Container
           ("muted yellow",
            [ Capacity(2, "shiny gold")
              Capacity(9, "faded blue") ])
         Container
           ("shiny gold",
            [ Capacity(1, "dark olive")
              Capacity(2, "vibrant plum") ])
         Container
           ("dark olive",
            [ Capacity(3, "faded blue")
              Capacity(4, "dotted black") ])
         Container
           ("vibrant plum",
            [ Capacity(5, "faded blue")
              Capacity(6, "dotted black") ])
         Container("faded blue", [])
         Container("dotted black", []) ]

  test
    <@ let actual = parseInput exampleInput
       actual = expected @>

[<Fact>]
let ``Helper function to list edges for example input test`` () =
  let input = parseInput exampleInput |> Result.get

  let expected =
    [ "light red", "bright white", 1
      "light red", "muted yellow", 2
      "dark orange", "bright white", 3
      "dark orange", "muted yellow", 4
      "bright white", "shiny gold", 1
      "muted yellow", "shiny gold", 2
      "muted yellow", "faded blue", 9
      "shiny gold", "dark olive", 1
      "shiny gold", "vibrant plum", 2
      "dark olive", "faded blue", 3
      "dark olive", "dotted black", 4
      "vibrant plum", "faded blue", 5
      "vibrant plum", "dotted black", 6
      "faded blue", "<end>", 0
      "dotted black", "<end>", 0 ]

  test <@ edges input = expected @>

[<Fact>]
let ``Part 1 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 4

  test <@ answer1 input = Ok expected @>

[<Fact>]
let ``Part 2 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 32

  let actual = answer2 input
  test <@ actual = Ok expected @>

let exampleInput2 = """
shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.
"""

[<Fact>]
let ``Part 2 answer for other example input test`` () =
  let input = parseInput exampleInput2 |> Result.get
  let expected = 126

  let actual = answer2 input
  test <@ actual = Ok expected @>
