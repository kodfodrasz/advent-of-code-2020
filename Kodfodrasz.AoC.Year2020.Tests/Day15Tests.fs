// fsharplint:disable ParameterNames
module Kodfodrasz.AoC.Year2020.Tests.Day15Tests

open Xunit
open Swensen.Unquote.Assertions

open Kodfodrasz.AoC
open Kodfodrasz.AoC.Year2020
open Kodfodrasz.AoC.Year2020.Day15


let exampleInput = """
  0,3,6
"""

[<Fact>]
let ``Parsing example input`` () =
  let expected: Result<_, string> = Ok [ 0; 3; 6 ]

  test
    <@ let actual = parseInput exampleInput
       actual = expected @>

[<Fact>]
let ``Part 1 helper function gameSeq for example input test (first 10 items)`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = [ 0; 3; 6; 0; 3; 3; 1; 0; 4; 0 ]

  test <@ gameSeq input |> Seq.take 10 |> Seq.toList = expected @>


[<Fact>]
let ``Part 1 answer for example input test`` () =
  let input = parseInput exampleInput |> Result.get
  let expected = 436

  test <@ answer1 input = Ok expected @>

[<Theory>]
[<InlineData("1,3,2", 1)>]
[<InlineData("2,1,3", 10)>]
[<InlineData("1,2,3", 27)>]
[<InlineData("2,3,1", 78)>]
[<InlineData("3,2,1", 438)>]
[<InlineData("3,1,2", 1836)>]
let ``Part 1 answer for example input test (various other example inputs)`` (input: string) (expected: int) =
  let input = parseInput input |> Result.get
  let expected: Result<_, string> = Ok expected

  test <@ answer1 input = expected @>

//[<Fact>]
//let ``Part 2 answer for example input test`` () =
//  let input = parseInput exampleInput |> Result.get
//  let expected = 1068781L

//  test <@ answer2 1L input = Ok expected @>
